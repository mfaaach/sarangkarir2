$(document).ready(function() {
  var allPanels = $('.panel').hide();
  var actives;
  var clicked = false;

  var book_name = $('#search_input').val();
  var url = "getbook/";

  $(document).on('click', '.accordion', function() {
    allPanels.slideUp();
    if(this === actives){
      $(this).next().slideUp();
      $(this).removeClass('actives');
      actives = null;
      return false;
    }
    $(actives).removeClass('actives');
    $(this).addClass('actives');
    $(this).next().slideDown();
    actives = this;
    return false;
  });

  load = function(book_name){
    $.ajax({
    type: "GET",
    url: url + book_name,
    dataType: 'json',
    success: function(response){
      $('#table_book > tbody').empty();
      data = response;
      book = data.book;

      for(i = 0; i < book.length; i++){
          var info = book[i]
          var tmp = "<tr><td id='title'>" + info.title + "</td><td>" + info.authors + "</td>" +
          "<td><img src="+info.images+"></td></tr>";
          $('#table_book > tbody').append(tmp);
        }
      }
    });
  }

  load('world company');

  $(document).on('click', '#search', function(){
    var book_name_local = $('#search_input').val();
    load(book_name_local);
  });

  $(document).keypress(function(e){
    if(e.which === 13){
      $('#search').click();
    }
  });
  return false;
});