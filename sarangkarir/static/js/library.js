$(document).ready(function(){
    // $.ajax({
    //   method: "GET",
    //   url: "https://www.googleapis.com/books/v1/volumes?q=writing",
    //   success: function(result){
    //     $("table").empty();
    //     $("table").append(
    //       '<thead>' +
    //         '<tr>' +
    //           '<th scope="col">Book Picture</th>' +
    //           '<th scope="col">Book Title</th>' +
    //           '<th scope="col">Book Author</th>' +
    //           '<th scope="col">Book Rating</th>' +
    //           '<th scope="col">Book Page Count</th>' +
    //           '<th scope="col">Book Description</th>' +
    //         '</tr>'+
    //       '</thead>'
    //     );
    //     for(i = 0; i < result.items.length ; i++){
    //       $("table").append(
    //         "<tbody>" +
    //           "<tr>" +
    //             "<th scope='row'>" + "<img src= '"+ result.items[i].volumeInfo.imageLinks.thumbnail + "'>" + "</td>" +
    //             "<td>" + result.items[i].volumeInfo.title + "</td>" +
    //             "<td>" + result.items[i].volumeInfo.authors + "</td>" +
    //             "<td>" + result.items[i].volumeInfo.ratingCount + "</td>" +
    //             "<td>" + result.items[i].volumeInfo.pageCount + "</td>" +
    //             "<td>" + result.items[i].volumeInfo.description + "</td>" +
    //           "</tr>" +
    //         "</tbody>"
    //       );
    //     }
    //   }
    // })
  
  
      $("button").click(
        function(){
          var keyword = $("#searchbook").val()
          console.log("start", keyword)
          $.ajax({
            method: "GET",
            url: "https://www.googleapis.com/books/v1/volumes?q="+keyword,
            success: function(result){
              $("table").empty();
              $("table").append(
                '<thead>' +
                  '<tr>' +
                    '<th scope="col">Book Picture</th>' +
                    '<th scope="col">Book Title</th>' +
                    '<th scope="col">Book Author</th>' +
                    '<th scope="col">Book Rating</th>' +
                    '<th scope="col">Book Page Count</th>' +
                    '<th scope="col">Book Description</th>' +
                  '</tr>'+
                '</thead>'
              );
              for(i = 0; i < result.items.length ; i++){
                $("table").append(
                  "<tbody>" +
                    "<tr>" +
                      "<th scope='row'>" + "<img src= '"+ result.items[i].volumeInfo.imageLinks.thumbnail + "'>" + "</td>" +
                      "<td>" + result.items[i].volumeInfo.title + "</td>" +
                      "<td>" + result.items[i].volumeInfo.authors + "</td>" +
                      "<td>" + result.items[i].volumeInfo.ratingCount + "</td>" +
                      "<td>" + result.items[i].volumeInfo.pageCount + "</td>" +
                      "<td>" + result.items[i].volumeInfo.description + "</td>" +
                    "</tr>" +
                  "</tbody>"
                );
              }
            }
          })
        }
      );
    })
    
    $("input").change(
      function(){
        console.log("start start")
      }
    );
  