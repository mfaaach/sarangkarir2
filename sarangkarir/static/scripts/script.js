$(function(){

    // jQuery methods go here...
    $('.toggle').click(function(e) {
        e.preventDefault();
    
      var $this = $(this);
    
      if ($this.next().hasClass('show')) {
          $this.next().removeClass('show');
          $this.next().slideUp(350);
      } else {
          $this.parent().parent().find('li .inner').removeClass('show');
          $this.parent().parent().find('li .inner').slideUp(350);
          $this.next().toggleClass('show');
          $this.next().slideToggle(350);
      }
    });
    $( ".inner-switch" ).on("click", function() {
        if( $( "body" ).hasClass( "dark" )) {
            $( "body" ).removeClass( "dark" );
            $( ".inner-switch" ).text( "OFF" );
            $( ".logoimg" ).attr('src', "/static/images/LogoBlack.png");
            $( ".btn-find" ).removeClass( "btn-outline-light" );
            $( ".btn-find" ).addClass( "btn-outline-dark" );
          
        } else {
          $( "body" ).addClass( "dark" );
          $( ".inner-switch" ).text( "ON" );
          $( ".logoimg" ).attr('src', "/static/images/sarangkarir Logo.png"); 
          $( ".btn-find" ).removeClass( "btn-outline-dark" );
          $( ".btn-find" ).addClass( "btn-outline-light" );
        }
    });

    $( ".inner-switch2" ).on("click", function() {
        if( $( ".findyournext" ).hasClass( "darkhome" )) {
            $( ".findyournext" ).removeClass( "darkhome" );
            $( ".findyournext" ).removeClass( "textdark" );
            $( ".container-FAQ" ).removeClass( "bg-dark" );
            $( ".container-FAQ" ).addClass( "bg-light" );
            $( ".change" ).removeClass( "textdark" );
            $( ".FAQ" ).removeClass( "darkhome" );
            $( ".FAQ" ).removeClass( "textdark" );
            $( ".inner-switch2" ).text( "OFF" );
            $( ".btn-find" ).removeClass( "btn-outline-light" );
            $( ".btn-find" ).addClass( "btn-outline-dark" );
          
        } else {
          $( ".findyournext" ).addClass( "darkhome" );
          $( ".findyournext" ).addClass( "textdark" );
          $( ".container-FAQ" ).addClass( "bg-dark" );
          $( ".container-FAQ" ).removeClass( "bg-light" );
          $( ".change" ).addClass( "textdark" );
          $( ".FAQ" ).addClass( "textdark" );
          $( ".FAQ" ).addClass( "darkhome" );
          $( ".inner-switch2" ).text( "ON" );
          $( ".btn-find" ).removeClass( "btn-outline-dark" );
          $( ".btn-find" ).addClass( "btn-outline-light" );
        }
    });

    $( "#anagram" ).hover(function() {
        if( $( "#anagram" ).hasClass( "temp" )) 
        {
            $( "#anagram" ).removeClass( "temp");
            $( "#anagram" ).attr('src', "/static/images/anagram.png");
        } 
        else 
        {
          $( "#anagram" ).addClass( "temp" );
          $( "#anagram" ).attr('src', "/static/images/anagram2.png");
        }
    });

});

$(function(){
    $.ajax({
        url: "data/?q=batman",
        datatype: 'json',
        success: function(data){
        $('tbody').html('')
        var result;
        
        for(var i = 0; i < data.items.length; i++) {
            var image = " ";

            if(data.items[i].volumeInfo.imageLinks != null){
                image = "<td><img class='img-fluid' style='width:22vh' src='" + data.items[i].volumeInfo.imageLinks.smallThumbnail +"'></img>" + "</td>";
            }
            result += "<tr><th scope='row' class='align-middle text-center'>" + (i+1) + "</th>" +
            image +
            "<td class='align-middle'>" + data.items[i].volumeInfo.title +"</td>" +
            "<td class='align-middle'>" + data.items[i].volumeInfo.authors + "</td>" + 
            "<td class='align-middle'>" + data.items[i].volumeInfo.publisher +"</td>" + 
            "<td class='align-middle'>" + data.items[i].volumeInfo.publishedDate +"</td>" + 
            "<td class='align-middle'>" + data.items[i].volumeInfo.categories +"</td></tr>";
        }
        $('tbody').append(result);
        console.log("testDone");
        }
    });
});

$(function(){
// jQuery methods go here...
$("#myInput").on("keyup", function(e) {
    q = e.currentTarget.value.toLowerCase()
    console.log(q)

    $.ajax({
        url: "data/?q=" + q,
        datatype: 'json',
        
        success: function(data){
        $('tbody').html('')
        var result;
        
        for(var i = 0; i < data.items.length; i++) {
            if(data.items[i].volumeInfo.imageLinks != null){
                result += "<tr><th scope='row' class='align-middle text-center'>" + (i+1) + "</th>" +
                "<td><img class='img-fluid' style='width:22vh' src='" + data.items[i].volumeInfo.imageLinks.smallThumbnail +"'></img>" + "</td>" +
                "<td class='align-middle'>" + data.items[i].volumeInfo.title +"</td>" +
                "<td class='align-middle'>" + data.items[i].volumeInfo.authors + "</td>" + 
                "<td class='align-middle'>" + data.items[i].volumeInfo.publisher +"</td>" + 
                "<td class='align-middle'>" + data.items[i].volumeInfo.publishedDate +"</td>" + 
                "<td class='align-middle'>" + data.items[i].volumeInfo.categories +"</td></tr>";
            }else{
                result += "<tr><th scope='row' class='align-middle text-center'>" + (i+1) + "</th>" +
                "<td class='align-middle'>" + data.items[i].volumeInfo.title +"</td>" +
                "<td class='align-middle'>" + data.items[i].volumeInfo.authors + "</td>" + 
                "<td class='align-middle'>" + data.items[i].volumeInfo.publisher +"</td>" + 
                "<td class='align-middle'>" + data.items[i].volumeInfo.publishedDate +"</td>" + 
                "<td class='align-middle'>" + data.items[i].volumeInfo.categories +"</td></tr>";
            }
        }
        $('tbody').append(result);
        console.log("testDone");
        }
    });
});
});