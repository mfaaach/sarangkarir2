from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from django.http import HttpResponse
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth import authenticate
from django.contrib.auth import logout as logout_bisa
import requests
import json

# Create your views here.
def index(request): 
    return render(request, 'books.html')

def data(request):
	try:
		q = request.GET['q']
	except:
		q = 'quilting'
	jsonInput = requests.get('https://www.googleapis.com/books/v1/volumes?q=' + q).json()
	return JsonResponse(jsonInput)