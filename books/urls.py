from django.urls import path
from . import views
from django.contrib.auth.views import LoginView, LogoutView

app_name = 'books'

urlpatterns = [
    path('', views.index, name='home'),
    path('data/', views.data, name='data'),
]