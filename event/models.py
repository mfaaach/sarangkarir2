from django.db import models
import datetime

class event(models.Model):
    dates = models.DateField()
    time = models.TimeField()
    eventname = models.CharField(max_length=50)
    eventdesc = models.CharField(max_length=500)
    location = models.CharField(max_length=50)
    rsvp = models.CharField(max_length=30)
