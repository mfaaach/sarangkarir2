# Generated by Django 2.2.6 on 2019-10-19 14:52

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('event', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='event',
            name='rsvp',
            field=models.CharField(max_length=30),
        ),
    ]
