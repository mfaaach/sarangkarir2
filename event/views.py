from django.shortcuts import render
from .forms import eventForm
from .models import event as jobfair
from django.shortcuts import redirect
import datetime
from django.http import HttpResponse
from django.http import HttpResponseRedirect
import json
from django.forms.models import model_to_dict
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse

url = 'https://www.googleapis.com/books/v1/volumes?q='

def sched_add(request):
    if request.method == 'POST':
        form = eventForm(request.POST)
        if form.is_valid():
            post = form.save(commit=False)
            post.save()
            return redirect('event:event')
    else:
        form = eventForm()

    content = {'title' : 'Form event',
                'form' : form}

    return render(request, 'sched_add.html', content)

def event(request):
    data = jobfair.objects.all()
    context = {
        'data' : data,
    }
    return render(request, 'event.html', context)

def sched_edit(request, pk):
    post = jobfair.objects.get(pk=pk)
    if request.method == "POST":
        form = eventForm(request.POST, instance=post)
        if form.is_valid():
            post = form.save(commit=False)
            post.save()
            return redirect('event:event')
    else:
        form = eventForm(instance=post)

    content = {'title' : 'Form event',
                'form' : form,
                'obj' : post}
    return render(request, 'sched_edit.html', content)

def sched_delete(request, pk):
    jobfair.objects.filter(pk=pk).delete()
    data = jobfair.objects.all()
    return redirect('event:event')

@csrf_exempt
def books(request):
	content = {'title' : 'Companies'}
	return render(request, 'event.html', content)

def readJSON(request, book_name):
	response = requests.get(url + book_name)
	data = response.json()
	books = {
		'book' : []
	}

	for item in data['items']:
		temp = {}
		temp['title'] = item['volumeInfo']['title']
		temp['authors'] = item['volumeInfo'].get('authors', ["Anonim"])[0]
		temp['images'] = item['volumeInfo']['imageLinks']['thumbnail']
		books['book'].append(temp)

	json_data = json.dumps(books)

	return HttpResponse(json_data, content_type="application/json")
