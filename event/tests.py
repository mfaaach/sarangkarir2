from django.test import TestCase, Client, LiveServerTestCase
from .models import event as jobfair
from .views import *
from .urls import *

class eventUnitTest(LiveServerTestCase):
    def test_url_exist(self):
        response = Client().get('/event/')
        self.assertEqual(response.status_code, 200)
    def test_url_not_exist(self):
        response = Client().get('/blank')
        self.assertEqual(response.status_code, 404)
    
#    def test_event_exists(self):
#        jobfair.objects.create(time="10:00:00")
#        jobfair.objects.create(eventname="Jobfair 2018")
#        jobfair.objects.create(eventdesc="Ini adalah Jobfair")
#        jobfair.objects.create(location="Jakarta")
#        jobfair.objects.create(rsvp="721-788392")
#        howmany = jobfair.objects.all().count()
#        self.assertEqual(howmany,5)

class TemplateAccess(LiveServerTestCase):
    def test_event_accessed(self):
        url = Client().get('/event/')
        self.assertTemplateUsed(url,'event.html')

    def test_event_accessed2(self):
        url2 = Client().get('/event/sched_add/')
        self.assertTemplateUsed(url2,'sched_add.html')

#    def test_event_accessed3(self):
#        url3 = Client().get('/event/sched_edit/7')
#        self.assertTemplateUsed(url3,'sched_edit.html')

