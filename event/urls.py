from .views import *
from django.shortcuts import render
from django.urls import path

app_name = 'event'

urlpatterns = [
    path('', event, name='event'),
    path('sched_add/', sched_add, name='sched_add'),
    path('sched_edit/<int:pk>', sched_edit, name='sched_edit'),
    path('sched_delete/<int:pk>', sched_delete, name='sched_delete'),
    path('getbook/<book_name>', readJSON, name='json'),
]
