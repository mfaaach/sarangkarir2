from django.shortcuts import render, redirect
from django.http import HttpResponse, JsonResponse
from django.core import serializers
from .models import Feedback
from . import forms

# Create your views here.
def contact(request):
    if request.method == 'POST':
        form = forms.FeedbackForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('contact:contact')

    else:
        form = forms.FeedbackForm()
    return render(request, 'contact.html', {'form': form, "list_feedback": Feedback.objects.all()})

def getAjax(request):
    if request.method == 'GET':
        data = serializers.serialize('json', Feedback.objects.all(), fields=("feedback"))
        data = eval(data)
        print(data)
        return JsonResponse(data, safe=False)
        
