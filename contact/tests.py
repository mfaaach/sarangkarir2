from django.test import TestCase, Client, LiveServerTestCase
from .models import Feedback
from .views import *
from .urls import *
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options   
from django.urls import reverse
import time


class ContactUnittest(TestCase):
    def test_url_exist(self):
        response = Client().get('/contact/')
        self.assertEqual(response.status_code, 200)

    def test_url_not_exist(self):
        response = Client().get('/blank')
        self.assertEqual(response.status_code, 404)

    def test_page(self):
        response = Client().get('/contact/')
        self.assertTemplateUsed(response, 'contact.html')

# class ContactFunctionalTest(LiveServerTestCase):
#     def setUp(self):
#         self.client = Client()
#         options = Options()
#         options.add_argument('--headless')
#         options.add_argument('--no-sandbox')
#         options.add_argument('--disable-dev-shm-usage')
#         self.browser = webdriver.Chrome('./chromedriver', chrome_options=options)

#     def tearDown(self):
#         self.browser.close()

#     def test_create_object(self):
#         self.browser.get(self.live_server_url + '/contact')    
#         time.sleep(1)
#         element = self.browser.find_element_by_id("id_feedback")
#         element.send_keys("Indonesia")
#         submit = self.browser.find_element_by_id("button")
#         submit.send_keys(Keys.RETURN)
#         time.sleep(3)
#         self.assertEqual(1, Feedback.objects.count())


#     def test_Feedback_exists(self):
#         Feedback.objects.create(feedback="test")
#         howmany = Feedback.objects.all().count()
#         self.assertEqual(howmany,1)

# class TemplateAccess(TestCase):
#     def test_Contact_accessed(self):
#         url = Client().get('/contact/')
#         self.assertTemplateUsed(url,'contact.html')
    
