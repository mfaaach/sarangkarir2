from django.test import TestCase, Client
from .models import Articles
from .views import *
from .urls import *


class ArticlesUnitTest(TestCase):
    def test_Articles_exists(self):
        Articles.objects.create(title="Test")
        Articles.objects.create(paragraph="Ini adalah Test")
        howmany = Articles.objects.all().count()
        self.assertEqual(howmany,2)

class TemplateAccess(TestCase):
    def test_Articles_accessed(self):
        url = Client().get('/article/articles/')
        self.assertTemplateUsed(url,'article.html')
        
    def test_Articles_accessed2(self):
        url = Client().get('/article/articles_create/')
        self.assertTemplateUsed(url,'article2.html')
