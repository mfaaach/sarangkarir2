from django.db import models

class Articles(models.Model):

    title = models.CharField(max_length=30, default="")
    paragraph = models.CharField(max_length=3000, default="")

