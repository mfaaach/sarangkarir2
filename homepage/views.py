from django.shortcuts import render
from .forms import EduForm
from .models import *
# Create your views here.
def index(request):
    return render(request, 'index.html')

def preference(request):
    if request.method == 'POST':
        form = request.POST
        # print(form['background'])

        result = Job.objects.filter(industry = form['industry'], background = form['background'], skill=form['skill'], wage=form['wage']).values()
        user = User.objects.create(name=form['name'])
        
        response = {
            'result':result,
            'user':user,
        }
        return render(request, "jobres.html", response)
        
    else :
        form = EduForm()
        return render(request, 'preference.html', {'form' : form})




    
