from django.urls import path
from .views import index, preference

app_name = 'homepage'

urlpatterns = [
    path('', index, name='index'),
    path('preference/', preference, name='preference'),
]