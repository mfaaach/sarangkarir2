# SarangKarir 2

```
Anggota Kelompok KD05 PPW 2019 :
Muhammad Fachri Anandito - 1806185393
Muhammad Ihsan Azizi - 1806186774
Stefan Kenichi - 1806191465
Tari Trinatha - 1806191080
```

## SarangKarir Heroku
[SarangKarir 2 Heroku](http://sarangkarir2.herokuapp.com/)

## Pipeline
[![pipeline status](https://gitlab.com/mfaaach/sarangkarir2/badges/master/pipeline.svg)](https://gitlab.com/mfaaach/sarangkarir2/commits/master)

## Coverage
[![coverage report](https://gitlab.com/mfaaach/sarangkarir2/badges/master/coverage.svg)](https://gitlab.com/mfaaach/sarangkarir2/commits/master)

## SarangKarir Description
<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SarangKarir adalah suatu perusahaan yang menyediakan layanan pencarian lowongan pekerjaan. Dua target utama dari website ini adalah para pencari pekerjaan dan pencari pekerja, namun tidak menutup kemungkinan buat pihak lain yang membutuhkan informasi-informasi faktual seputar pekerjaan ataupun kondisi pekerja dalam ranah yang luas, yaitu negara. Untuk memenuhi kebutuhan dari masing-masing target visitor, SarangKarir pun menyediakan dua fitur utama: pencari pekerja dan artikel sesuai dengan interest yang diberikan oleh user. Untuk menggunakan fitur ini, pertama-tama user harus sign in jika sudah punya akun, atau sign up jika belum memiliki akun. Kemudian, user akan diarahkan kepada form-form yang harus diisi untuk mendeskripsikan interest yang dimaksud oleh user. Form-form ini antara lain adalah educational background, industry, skill, dan desired wage. Setelah itu, kami akan memberikan info lowongan pekerjaan yang sekiranya terkait dengan seluruh requirement tersebut. Kami juga memberikan lowongan-lowongan pekerjaan lainnya yang masih relatable sehingga user dapat memiliki pilihan. Tak lupa, kami juga menyediakan fitur untuk meminta feedback atau pertanyaan dari user yang dapat dikirimkan kepada kami melalui e-mail mereka. Pertanyaan-pertanyaan ini nantinya akan memengaruhi daftar pertanyaan pada Frequently Asked Question. Sementara itu, untuk artikelnya, kami menyediakan gambar-gambar yang mendeskripsikan pekerjaan yang sedang dibicarakan di artikel itu. User dapat mengeklik salah satu gambar untuk melihat informasi pekerjaan apa yang terkandung di baliknya. Tak lupa, kami juga menyajikan data-data faktual untuk keperluan riset pada bidang sumber daya manusia/tenaga kerja di Indonesia.</p>

## SarangKarir Features
* Homepage 
* Terdapat fitur sign-in & sign-up. Masing-masing fitur mempunyai page yang berbeda. Fitur sign-up diperuntukkan bagi user yang belum mempunyai akun. User akan diminta input email/username & password untuk sign-in.
* Question page yang berisi pertanyaan-pertanyaan yang sering diajukan tentang web kami.
* Contact page yang berisi beberapa sosial media web kami dan juga textbox untuk menulis pesan/saran.
* Form preference untuk finding job berdasarkan : educational background, industry, skill, dan range sallary yang diinginkan.
* Form untuk menulis artikel.

## Division of Task
```
Fachri
- Homepage
- Find Job Page including Result Job Page
Stefan
- Article Page
Ihsan
- Page Contact Us
Tari
- Page Jobfair
```






