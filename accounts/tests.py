from django.test import TestCase

# Create your tests here.
from django.contrib.auth import authenticate
from django.contrib.auth.models import User
from django.http import request
from importlib import import_module
from django.test import TestCase, LiveServerTestCase
from django.test import Client
from django.urls import resolve
from django.conf import settings

from .views import *

# Create your tests here.

class AccountUnitTest(TestCase):

    def test_account_url_is_exist(self):
        response = Client().get('/accounts/')
        self.assertEqual(response.status_code, 200)

    def test_account_url_doesnt_exist(self):
        response = Client().get('/wek')
        self.assertEqual(response.status_code, 404)

    def test_account_using_index_template(self):
        response = Client().get('/accounts/')
        self.assertTemplateUsed(response, 'index1.html')

    def test_login_url_is_exist(self):
        response = Client().get('/accounts/login/')
        self.assertEqual(response.status_code, 200)

    def test_register_url_is_exist(self):
        response = Client().get('/accounts/register/')
        self.assertEqual(response.status_code, 200)

    def test_register_post_using_register_template(self):
        response = Client().post('/accounts/register/', {'username':'mfaaach'})
        self.assertTemplateUsed(response, 'register.html')

    # def test_data_url_is_exist(self):
    #     response = Client().get('/data/')
    #     self.assertEqual(response.status_code, 200)

    # def test_library_using_index_template(self):
    #     response = Client().get('/')
    #     self.assertTemplateUsed(response, 'index.html')