from django.urls import path
from . import views
from django.contrib.auth.views import LoginView, LogoutView

app_name = 'accounts'

urlpatterns = [
    path('', views.indexView, name='home'),
    path('login/', LoginView.as_view(template_name='login.html', redirect_field_name='homepage:index'), name = 'login_url'), 
    path('register/', views.register, name = 'register_url'),
    path('logout/', LogoutView.as_view(next_page='homepage:index'), name = 'logout'),
]